module.exports = {
    root: true,
    parser: 'vue-eslint-parser',
    parserOptions: {
        parser: 'babel-eslint',
        sourceType: 'module',
    },
    env: {
        browser: true,
    },
    extends: [
        'airbnb-base',
        'plugin:vue/recommended',
    ],
    plugins: [
        'vue',
    ],
    rules: {
        'linebreak-style': [
            'off',
        ],
        indent: [
            'off',
        ],
        'vue/html-indent': [
            'error',
            4,
            {
                alignAttributesVertically: false,
            },
        ],
        'newline-per-chained-call': [
            'error',
            {
                ignoreChainWithDepth: 3,
            },
        ],
        'no-console': [
            'off',
        ],
        'no-plusplus': [
            'error',
            {
                allowForLoopAfterthoughts: true,
            },
        ],
        'no-mixed-operators': [
            'off',
        ],
        'no-restricted-globals': [
            'off',
        ],
        'vue/singleline-html-element-content-newline': [
            'off',
        ],
        'no-underscore-dangle': [
            0,
        ],
        'max-len': [
            'error',
            120,
            2,
            {
                ignoreUrls: true,
                ignoreComments: false,
                ignoreRegExpLiterals: true,
                ignoreStrings: true,
                ignoreTemplateLiterals: true,
            },
        ],
        'vue/max-attributes-per-line': [
            'error',
            {
                singleline: 10,
                multiline: {
                    max: 1,
                    allowFirstLine: true,
                },
            },
        ],
        'vue/order-in-components': [
            'error',
            {
                order: [
                    'el',
                    'name',
                    'parent',
                    'functional',
                    [
                        'delimiters',
                        'comments',
                    ],
                    'extends',
                    'mixins',
                    'inheritAttrs',
                    [
                        'components',
                        'directives',
                        'filters',
                    ],
                    'model',
                    [
                        'props',
                        'propsData',
                    ],
                    'fetch',
                    'asyncData',
                    'data',
                    'computed',
                    'methods',
                    'head',
                    'watch',
                    'LIFECYCLE_HOOKS',
                    [
                        'template',
                        'render',
                    ],
                    'renderError',
                ],
            },
        ],
        'no-param-reassign': [
            'error',
            {
                'props': true,
                'ignorePropertyModificationsFor': [
                    'state',
                ],
            },
        ],
        'import/no-unresolved': [
            'off',
        ]
    },
};
