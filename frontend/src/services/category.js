import Axios from 'axios';
import appDefaults from '../config';

const categoryApi = () => Axios.create({
    baseURL: appDefaults.routes.categoryRoute,
});

export default categoryApi();
