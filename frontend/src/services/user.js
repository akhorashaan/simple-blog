import Axios from 'axios';
import appDefaults from '../config';

const userApi = () => Axios.create({
    baseURL: appDefaults.routes.userRoute,
});

export default userApi();
