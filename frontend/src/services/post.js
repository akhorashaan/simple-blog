import Axios from 'axios';
import appDefaults from '../config';

const postApi = () => Axios.create({
    baseURL: appDefaults.routes.postRoute,
});

export default postApi();
