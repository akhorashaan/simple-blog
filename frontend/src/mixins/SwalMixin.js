import Swal from 'sweetalert2';

const SweetAlert = {
    methods: {
        async alert(options) {
            this.$store.commit('BLOCK_CONTENT', true);
            await Swal.fire(options);
            this.$store.commit('BLOCK_CONTENT', false);
        },
        async alertError(text) {
            await this.alert({
                title: 'Ошибка!',
                text,
                icon: 'error',
                toast: true,
                timer: 500000,
                showConfirmButton: true,
            });
        },
        async alertSuccess(text) {
            await this.alert({
                title: 'Успешно!',
                text,
                icon: 'success',
                toast: true,
                showConfirmButton: true,
                timer: 10100000,
                timerProgressBar: true,
            });
        },
    },
};

export default SweetAlert;
