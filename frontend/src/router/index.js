import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        component: () => import('../views/BlogTemplate.vue'),
        children: [
            {
                path: '/',
                name: 'Home',
                component: () => import('../views/Blog/MainPage.vue'),
            },
            {
                path: '/post/all',
                name: 'Posts',
                component: () => import('../views/Blog/PostsPage.vue'),
            },
            {
                path: '/post/:slug',
                name: 'Post',
                component: () => import('../views/Blog/PostPage.vue'),
            },
            {
                path: '/categories',
                name: 'Categories',
                component: () => import('../views/Blog/CategoriesPage.vue'),
            },
            {
                path: '/category/:slug',
                name: 'Category',
                component: () => import('../views/Blog/CategoryPage.vue'),
            },
            {
                path: '/search',
                name: 'Search',
                component: () => import('../views/Blog/SearchPage.vue'),
            },
            {
                path: '/search/:query',
                name: 'Search With Query',
                component: () => import('../views/Blog/SearchPage.vue'),
            },
            {
                path: '/auth',
                name: 'AuthTemplate',
                component: () => import('../views/Blog/Auth/AuthTemplate.vue'),
                children: [
                    {
                        path: '/signin/:jwt',
                        name: 'Signin Status',
                        component: () => import('../views/Blog/Auth/SigninPage.vue'),
                        meta: { onlyGuest: true },
                    },
                    {
                        path: '/signin',
                        name: 'Signin',
                        component: () => import('../views/Blog/Auth/SigninPage.vue'),
                        meta: { onlyGuest: true },
                    },
                    {
                        path: '/signup',
                        name: 'Signup',
                        component: () => import('../views/Blog/Auth/SignupPage.vue'),
                        meta: { onlyGuest: true },
                    },
                ],
            },
        ],
    },
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
    scrollBehavior() {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve({ x: 0, y: 0 });
            }, 500);
        });
    },
});

router.beforeEach(async (to, from, next) => {
    if (to.matched.some((record) => record.meta.onlyGuest)) {
        if (window.sessionStorage.getItem('payload')) {
            next({
                path: '/',
            });
        } else {
            next();
        }
    } else {
        next();
    }
});

export default router;
