const serverPath = 'http://192.168.1.217:3000';
const apiPath = `${serverPath}/api/v1/`;

const routes = {
    imageRoute: serverPath,
    postRoute: `${apiPath}post`,
    categoryRoute: `${apiPath}category`,
    userRoute: `${apiPath}user`,
};

const appDefaults = { routes };

export default appDefaults;
