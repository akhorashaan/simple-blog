import Vue from 'vue';
import Vuex from 'vuex';
import VueJwtDecode from 'vue-jwt-decode';

import postApi from '../services/post';
import userApi from '../services/user';
import categoryApi from '../services/category';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        userID: '',
        username: '',
        categories: [],
        heroPosts: [],
        topPosts: [],
        contentIsBlocked: false,
    },
    getters: {
        USER_ID: (state) => state.userID,
        USERNAME: (state) => state.username,
        CATEGORIES: (state) => state.categories,
        HERO_POSTS: (state) => state.heroPosts,
        TOP_POSTS: (state) => state.topPosts,
        CONTENT_BLOCKED: (state) => state.contentIsBlocked,
    },
    mutations: {
        SET_USERNAME: (state, payload) => {
            state.username = payload;
        },
        SET_USER_ID: (state, payload) => {
            state.userID = payload;
        },
        SET_CATEGORIES: (state, payload) => {
            state.categories = payload;
        },
        SET_HERO_POSTS: (state, payload) => {
            state.heroPosts = payload;
        },
        SET_TOP_POSTS: (state, payload) => {
            state.topPosts = payload;
        },
        BLOCK_CONTENT: (state, payload) => {
            state.contentIsBlocked = payload;
        },
    },
    actions: {
        async fetchCategories(context) {
            try {
                const { data } = await categoryApi.post('getallcategories');

                context.commit('SET_CATEGORIES', data);
            } catch (e) {
                context.commit('SET_CATEGORIES', []);
            }
        },
        async fetchHeroPosts(context) {
            try {
                const { data } = await postApi.post('getpostsforhero');

                context.commit('SET_HERO_POSTS', data);
            } catch (e) {
                context.commit('SET_HERO_POSTS', []);
            }
        },
        async fetchTopPosts(context) {
            try {
                const { data } = await postApi.post('gettopposts');

                context.commit('SET_TOP_POSTS', data);
            } catch (e) {
                context.commit('SET_TOP_POSTS', []);
            }
        },
        async fetchUser(context) {
            if (window.sessionStorage.payload) {
                try {
                    const _id = { _id: VueJwtDecode.decode(window.sessionStorage.payload).user._id };
                    const { data } = await userApi.post('getuser', _id);

                    if (data) {
                        context.commit('SET_USERNAME', data.name);
                        context.commit('SET_USER_ID', data._id);
                    }
                } catch (e) {
                    console.log(e);
                }
            }
        },
        signoutUser(context) {
            context.commit('SET_USERNAME', '');
            context.commit('SET_USER_ID', '');
            window.sessionStorage.setItem('payload', '');
        },
    },
});
