import Vue from 'vue';

import App from './App.vue';
import router from './router';
import store from './store';

import appDefaults from './config';
import alert from './mixins/SwalMixin';

Vue.config.productionTip = false;

Vue.prototype.$thumbnail = (img, width, height) => `${appDefaults.routes.imageRoute}/thumb/?src=${img}&width=${width}&height=${height}`;
Vue.mixin(alert);

new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount('#app');
