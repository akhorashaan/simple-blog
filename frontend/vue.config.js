module.exports = {
    css: {
        loaderOptions: {
            sass: { prependData: `@import "./src/mixins/scss/_defaults.scss";` },
        },
    },
};
