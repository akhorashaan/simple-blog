const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    pwd: {
        type: String,
        default: '',
    },
    date: {
        type: Date,
        default: Date.now,
    },
    source: {
        type: String,
        default: 'local',
    },
    avatar: {
        type: String,
        default: 'https://peopletalk.ru/wp-content/uploads/2016/11/1480331127.jpg',
    }
});

const User = mongoose.model('User', UserSchema, 'users');

module.exports = User;
