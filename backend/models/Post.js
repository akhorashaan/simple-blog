const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PostSchema = new Schema({
    name: {
        type: 'String',
        required: [true, 'Post name must be string!'],
    },
    slug: {
        type: 'String',
        required: [true, 'Post slug must be string!'],
    },
    image: {
        type: 'String',
    },
    lead: {
        type: 'String',
    },
    body: {
        type: 'String',
    },
    date: {
        type: 'Date',
        required: [true, 'Date must be in date format'],
    },
    inSlider: {
        type: 'Boolean',
        default: false,
    },
    category: { type: Schema.Types.ObjectId, ref: 'Category' },
    readCounter: {
        type: 'Number',
        default: 0,
    },
    readers: [{ type: Array }],
});

PostSchema.index({
    name: 'text',
    lead: 'text',
    body: 'text',
});

const Post = mongoose.model('Post', PostSchema, 'posts');

module.exports = Post;
