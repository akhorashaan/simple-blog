const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const ReadersLogSchema = new Schema({
    ip: {
        type: String,
        required: true,
    },
    post: {
        type: Schema.Types.ObjectId,
        ref: 'Post',
        required: true,
    },
    date: {
        type: Date,
        default: Date.now,
        required: true,
    },
});

const ReadersLog = mongoose.model('ReadersLog', ReadersLogSchema, 'readers_log');

module.exports = ReadersLog;
