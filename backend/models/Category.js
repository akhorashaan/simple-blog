const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CategorySchema = new Schema({
    name: {
        type: 'String',
        required: [true, 'Category name must be string!'],
    },
    slug: {
        type: 'String',
        required: [true, 'Category slug must be string!'],
    },
    image: {
        type: 'String',
    },
});

const Category = mongoose.model('Category', CategorySchema, 'category');

module.exports = Category;
