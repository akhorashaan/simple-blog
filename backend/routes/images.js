const express = require('express');
const router = express.Router();

const thumbnail = require('../services/thumbnail');


/* GET image thumb. */
router.get('/', thumbnail.sendThumb);

module.exports = router;
