const express = require('express');
const router = express.Router();

const UserController = require('../controllers/user_controller');
const googleStrategy = require('../services/strategies/google');
const localStrategy = require('../services/strategies/local');
const vkStrategy = require('../services/strategies/vk');
const facebookStrategy = require('../services/strategies/facebook');

router.post('/signup', localStrategy.signUp);

router.post('/signin', localStrategy.signIn);

router.get('/auth/google', googleStrategy.googleAuth);

router.get('/auth/google/callback', googleStrategy.googleAuthCallback);

router.get('/auth/vk', vkStrategy.vkAuth);

router.get('/auth/vk/callback', vkStrategy.vkAuthCallback);

router.get('/auth/facebook', facebookStrategy.facebookAuth);

router.get('/auth/facebook/callback', facebookStrategy.facebookAuthCallback);

router.post('/getuser', UserController.getUser);

module.exports = router;
