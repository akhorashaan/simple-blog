const express = require('express');
const router = express.Router();

const postController = require('../controllers/post_controller');

router.post('/getallposts', postController.getAllPosts);

router.post('/getpostsforhero', postController.getPostsForHero);

router.post('/getnewestposts', postController.getNewestPosts);

router.post('/gettopposts', postController.getTopPosts);

router.post('/getpostbyslug', postController.getPostBySlug);

router.post('/getpostsbyquery', postController.getPostsByQuery);

router.post('/getpostsbycategory', postController.getPostsByCategory);

module.exports = router;
