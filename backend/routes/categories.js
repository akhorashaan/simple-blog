const express = require('express');
const router = express.Router();

const categoryController = require('../controllers/category_controller');

router.post('/getallcategories', categoryController.getAllCategories);

module.exports = router;
