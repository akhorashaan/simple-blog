const categoryRepository = require('../repositories/category-repository');

exports.getAllCategories = async (req, res) => {
    try {
        const categories = await categoryRepository.getAllCategorues();

        if (categories && categories.length !== 0) {
            res.send(categories);
        } else {
            res.status(400);
            res.send({ errorMessage: 'Не найдены записи' });
        }
    }
    catch (e) {
        res.status(500);
        res.send({ errorMessage: 'Не удалось получить записи' });
    }
};
