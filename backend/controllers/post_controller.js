const Post = require('../models/Post');
const ReadersLog = require('../models/ReadersLog');

const Axios = require('axios');
const multer = require('multer');
const fs = require('fs');
const gm = require('gm');
const moment = require('moment');
const mongoose = require('mongoose');

const currentDateString = new Date();
const year = currentDateString.getFullYear();
const month = currentDateString.getMonth();
const day = currentDateString.getDate();
const pathToUpload = `./uploads/post/${year}/${month}/${day}/`;
const pathForDatabase = `post/${year}/${month}/${day}`;
let filenameToUpload = '';
let fileExtension = '';

const postRepository = require('../repositories/post-repository');

const sendRes = (data, res) => {
    if (data && data.length !== 0) {
        res.send(data);
    } else {
        res.status(400);
        res.send({ errorMessage: 'Не удалось получить записи' });
    }
}

const sendCriticalError = (res) => {
    res.status(500);
    res.send({ errorMessage: 'Ошибка сервера' });
}

exports.getAllPosts = async (req, res) => {
    try {
        const page = parseInt(req.body.page) || 0;
        const posts = await postRepository.getPostsByPage(null, null, page);

        sendRes(posts, res);
    } catch (err) {
        sendCriticalError(res);
    }
};

exports.getPostsByCategory = async (req, res) => {
    try {
        const page = parseInt(req.body.page) || 0;
        const id = mongoose.Types.ObjectId(req.body.id);
        const posts = await postRepository.getPostsByPage('category', id, page);

        sendRes(posts, res);
    } catch (err) {
        sendCriticalError(res);
    }
};

exports.getNewestPosts = async (req, res) => {
    try {
        const posts = await postRepository.getPostsByPage(null, null, 0);

        sendRes(posts, res);
    } catch (e) {
        sendCriticalError(res);
    }
};

exports.getPostsByQuery = async (req, res) => {
    try {
        const posts = await postRepository.getPostsBySearch(req.body.query);

        sendRes(posts, res);
    } catch (err) {
        sendCriticalError(res);
    }
};

exports.getPostsForHero = async (req, res) => {
    try {
        const posts = await postRepository.getPostsForHero()

        sendRes(posts, res);
    } catch (e) {
        sendCriticalError(res);
    }
};

exports.getTopPosts = async (req, res) => {
    try {
        const posts = await postRepository.getTopReadedPosts();

        sendRes(posts, res);
    } catch (e) {
        sendCriticalError(res);
    }
};

exports.getPostBySlug = async (req, res) => {
    try {
        const userIp = req.ip;
        const post = await postRepository.getPost(req.body.slug);

        postRepository.updatePostReaders(userIp, post._id);
        sendRes(post, res);
    } catch (e) {
        sendCriticalError(res);
    }
};/*

exports.createPost = (req, res) => {

    const post = new Post({
        name: 'temp',
        lead: 'temp',
        body: {
            text: 'temp',
        },
        date: new Date(),
    });

    post.save((e, post) => {
        if (!e) {
            res.send(post._id);
        } else {
            res.status(500);
            res.send('Не удалось создать новую запись');
        }
    });
};

exports.updatePostById = async (req, res) => {
    const post = req.body;

    await Post
        .findByIdAndUpdate({ _id: post._id }, {
                name: post.name,
                lead: post.lead,
                body: post.body,
                date: post.date || new Date(),
            }, null,
            (err) => {
                if (!err) {
                    res.send('Post updated');
                } else {
                    res.status(500);
                    res.send({ errorMessage: 'Что-то пошло не так' });
                }
            });
};

exports.deletePostById = async (req, res) => {
    const postId = req.body.id;

    await Post
        .deleteOne({ _id: postId }, (err) => {
            if (!err) {
                res.status(200);
                res.send('Успешно удалено');
            } else {
                res.status(500);
            }
        });

};

// Image file uploading functions //
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        fs.mkdirSync(pathToUpload, { recursive: true });
        cb(null, pathToUpload);
    },
    filename: function (req, file, cb) {
        filenameToUpload = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 10);
        fileExtension = file.originalname.substring(file.originalname.lastIndexOf('.'), file.originalname.length);

        cb(null, filenameToUpload + fileExtension);
    },
});

const uploader = multer({
    storage: storage,
    limits: { fileSize: 1000000 },
}).single('file');

exports.PostUploadImage = (req, res) => {
    uploader(req, res, (err) => {
        if (err instanceof multer.MulterError) {
            res.status(500);
            res.send({ errorMessage: 'Ошибка загрузки: ' + err.message });
        } else if (err) {
            res.status(500);
            res.send({ errorMessage: 'Неизвестная ошибка' });
        } else {
            const image = pathToUpload + filenameToUpload + fileExtension;

            gm(image).size((err, val) => {
                if (val.width < 1000 || val.height < 500) {
                    res.status(500);
                    res.send({ errorMessage: 'Ширина изображения должна быть больше 1000, а высота - 500 пикселей' });

                    fs.unlinkSync(image);
                } else {
                    createThumbnail(image, 1000, 300);
                    createThumbnail(image, 300, 100);

                    Post.findByIdAndUpdate({ _id: req.body._id }, {
                        image: pathForDatabase + '/' + filenameToUpload + fileExtension,
                    }, null, () => {
                    });

                    res.status(200);
                    res.send({ image: pathForDatabase + '/' + filenameToUpload + fileExtension });
                }
            });
        }
    });
};

// Image URL uploading functions //
async function downloadImage(url) {
    fs.mkdirSync(pathToUpload, { recursive: true });

    filenameToUpload = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 10);
    const writer = fs.createWriteStream(pathToUpload + filenameToUpload + '.jpg');

    const response = await Axios({
        url,
        method: 'GET',
        responseType: 'stream',
    });

    response.data.pipe(writer);

    return new Promise((resolve, reject) => {
        writer.on('finish', resolve);
        writer.on('error', reject);
    });
}

exports.uploadByUrl = (req, res) => {
    fileExtension = '.jpg';

    downloadImage(req.body.url)
        .then(() => {
            const image = pathToUpload + filenameToUpload + fileExtension;

            gm(image).size((err, val) => {
                if (val.width < 1000 || val.height < 500) {
                    res.status(500);
                    res.send({ errorMessage: 'Ширина изображения должна быть больше 1000, а высота - 500 пикселей' });

                    fs.unlinkSync(image);
                } else {
                    createThumbnail(image, 1000, 300).then();
                    createThumbnail(image, 300, 100).then();

                    Post.findByIdAndUpdate({ _id: req.body._id }, {
                        image: pathForDatabase + '/' + filenameToUpload + fileExtension,
                    }, null, () => {
                    });

                    res.status(200);
                    res.send({ image: pathForDatabase + '/' + filenameToUpload + fileExtension });
                }
            });
        })
        .catch((e) => {
            res.status(500);
            res.send({ errorMessage: 'Загрузка изображения не удалась' });
        });
};

async function createThumbnail(image, width, height) {
    await gm(image)
        .gravity('Center')
        .thumb(width, height, `${image}_thumb${width}`, 80, function (err) {
            if (err) throw err;
        });
}
*/
