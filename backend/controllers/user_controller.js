const userRepository = require('../repositories/user-repository');

exports.getUser = async (req, res) => {
    try {
        const user = await userRepository.findUser(req.body);

        res.status(200);
        res.send(user);
    } catch (e) {
        res.status(500);
        res.send('Неизвестная ошибка');
    }
};



