module.exports = {
    async up(db) {
        await db.collection('category').insertOne({ name: 'Без категории', slug: 'default', image: '' });
    },

    async down(db) {
        await db.collection('category').deleteOne(
            { name: 'Без категории', slug: 'default' },
        );
    },
};
