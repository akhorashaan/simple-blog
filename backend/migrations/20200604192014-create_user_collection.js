module.exports = {
    async up(db, client) {
        await db.createCollection('users', {
            validator: {
                $jsonSchema: {
                    bsonType: 'object',
                    required: ['email', 'pwd'],
                    properties: {
                        name: {
                            bsonType: 'string',
                            description: 'Username must be string!',
                        },
                        email: {
                            bsonType: 'string',
                            description: 'Password must be string!',
                        },
                        date: {
                            bsonType: 'date',
                            description: 'Date must be in date format',
                        },
                        source: {
                            bsonType: 'string',
                        },
                        avatar: {
                            bsonType: 'string',
                        }
                    },
                },
            },
        });

        await db.collection('users').createIndex({name: 1}, {unique: true});
        await db.collection('users').createIndex({email: 1}, {unique: true});
    },
};
