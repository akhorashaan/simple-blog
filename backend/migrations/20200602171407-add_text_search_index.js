module.exports = {
    async up(db) {
        await db.collection('posts').createIndex(
            {
                name: 'text',
                lead: 'text',
                body: 'text',
            },
            { default_language: 'russian' },
        );
    },
};
