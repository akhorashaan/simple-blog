module.exports = {
    async up(db) {
        await db.createCollection('readers_log', {
            validator: {
                $jsonSchema: {
                    bsonType: 'object',
                    properties: {
                        ip: {
                            bsonType: 'string',
                            description: 'Category ip must be string!',
                        },
                        post: {
                            bsonType: 'string',
                            description: 'Category objectId must be string!',
                        },
                        date: {
                            bsonType: 'date',
                            description: 'Category date must be string!',
                        },
                    },
                },
            },
        });
    },
};
