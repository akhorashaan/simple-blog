const createPostsCollection = async (db) => {
    await db.createCollection('posts', {
        validator: {
            $jsonSchema: {
                bsonType: 'object',
                required: ['name', 'date'],
                properties: {
                    name: {
                        bsonType: 'string',
                        description: 'Post name must be string!',
                    },
                    slug: {
                        bsonType: 'string',
                        description: 'Post slug must be string!',
                    },
                    image: {
                        bsonType: 'string',
                    },
                    lead: {
                        bsonType: 'string',
                        description: 'Post lead must be string!',
                    },
                    body: {
                        bsonType: 'string',
                        description: 'Test message for post body error',
                    },
                    date: {
                        bsonType: 'date',
                        description: 'Date must be in date format',
                    },
                },
            },
        },
    });
};

module.exports = {
    async up(db) {
        try {
            const col = await db.listCollections({ name: 'posts' }).toArray();

            if (col.length > 0) {
                throw new Error('Collection Posts is already presents. Exiting...');
            } else {
                await createPostsCollection(db);
            }
        } catch (e) {
            throw e;
        }
    },

    async down(db) {
        try {
            await db.dropCollection('posts');
        } catch (e) {
            throw e;
        }
    },
};
