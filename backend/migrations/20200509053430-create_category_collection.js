const createPostsCollection = async (db) => {
    await db.createCollection('category', {
        validator: {
            $jsonSchema: {
                bsonType: 'object',
                required: ['name'],
                properties: {
                    name: {
                        bsonType: 'string',
                        description: 'Category name must be string!',
                    },
                    slug: {
                        bsonType: 'string',
                        description: 'Category name must be string!',
                    },
                    slug: {
                        bsonType: 'string',
                        description: 'Category name must be string!',
                    },
                },
            },
        },
    });
};

module.exports = {
    async up(db) {
        try {
            const col = await db.listCollections({ name: 'category' }).toArray();

            if (col.length > 0) {
                throw new Error('Collection Category is already presents. Exiting...');
            } else {
                await createPostsCollection(db);
            }
        } catch (e) {
            throw e;
        }
    },

    async down(db) {
        try {
            await db.dropCollection('category');
        } catch (e) {
            throw e;
        }
    },
};
