require('dotenv').config();

const config = {
    mongodb: {
        url: `mongodb://${process.env.DB_USER}:${process.env.DB_PWD}@${process.env.DB_HOST}:${process.env.DB_PORT}`,
        databaseName: 'simple-blog',
        options: {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        },
    },
    migrationsDir: 'populations',
    changelogCollectionName: 'changelog',
};

module.exports = config;
