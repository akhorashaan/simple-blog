const Category = require('../models/Category');

exports.getAllCategorues = () => {
    return Category
        .aggregate()
        .lookup({
            from: 'posts',
            localField: '_id',
            foreignField: 'category',
            as: 'posts',
        })
        .project({
            name: 1,
            slug: 1,
            image: 1,
            posts: { $size: { '$ifNull': ['$posts', []] } }
        });
}
