const User = require('../models/User');

exports.findUser = (_id) => {
    return User.findOne({ _id });
}
