const Post = require('../models/Post');
const ReadersLog = require('../models/ReadersLog');

const moment = require('moment');
const beginOfWeek = moment().startOf('week');

exports.getPostsByPage = (criteria, param, page) => {
    const query = (criteria !== null && param !== null) ? { [criteria]: param } : {};
    return Post
        .aggregate()
        .match(query)
        .sort({ 'date': -1 })
        .skip(page * 12)
        .limit(12)
        .lookup({
            from: 'category',
            localField: 'category',
            foreignField: '_id',
            as: 'category'
        })
        .unwind('$category')
        .lookup({
            from: 'readers_log',
            let: { 'postId': '$_id' },
            pipeline: [
                { $match: { '$expr': { '$eq': ['$$postId', '$post'] } } },
                { $count: 'count' }
            ],
            as: 'readersCounter'
        })
        .unwind('$readersCounter')
        .project({
            name: 1,
            slug: 1,
            lead: 1,
            image: 1,
            date: 1,
            category: '$category.name',
            readersCounter: '$readersCounter.count',
        });
}

exports.getPostsBySearch = (query) => {
    return Post
        .find({ $text: { $search: query } }, 'name lead date slug')
        .limit(50)
        .lean();
}

exports.getPostsForHero = () => {
    return Post
        .find({ inSlider: true }, 'name lead image slug')
        .sort({ date: -1 })
        .lean();
}

exports.getTopReadedPosts = () => {
    return ReadersLog
        .aggregate()
        .match({
            date: { $gte: beginOfWeek.toDate() }
        })
        .sortByCount('$post')
        .limit(5)
        .lookup({
            from: 'posts',
            localField: '_id',
            foreignField: '_id',
            as: 'post',
        })
        .unwind('$post')
        .project({
            name: '$post.name',
            slug: '$post.slug',
            lead: '$post.lead',
            image: '$post.image',
            count: 1,
        })
}

exports.getPost = (query) => {
    return Post
        .findOne({ slug: query })
        .lean()
        .exec();
}

exports.updatePostReaders = async (readerIp, post) => {
    try {
        const alreadyReaded = await ReadersLog.find({
            ip: readerIp,
            post: post,
        });

        if (alreadyReaded.length > 0) {
            return;
        }

        ReadersLog.create({
            ip: readerIp,
            post: post,
            date: Date.now(),
        })
    } catch (e) {
        console.log(e);
    }
}
