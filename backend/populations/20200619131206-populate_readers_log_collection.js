const getPostId = async (db) => {
    const id = await db.collection('posts').aggregate([{ $sample: { size: 1 } }]).toArray();

    return id[0]._id;
};

const getDate = () => {
    return new Date();
};

module.exports = {
    async up(db) {
        for (let i = 0; i < 1000; i++) {
            const id = await getPostId(db);

            await db.collection('readers_log').insertOne({
                ip: '192.168.1.1',
                post: id,
                date: getDate(),
            })
        }
    },
};
