const Axios = require('axios');
const fs = require('fs');

const currentDateString = new Date();
const year = currentDateString.getFullYear();
const month = currentDateString.getMonth();
const day = currentDateString.getDate();
const pathToUpload = `./uploads/post/${year}/${month}/${day}/`;
const pathForDatabase = `post/${year}/${month}/${day}`;
let filenameToUpload = '';

async function downloadImage(url) {
    fs.mkdirSync(pathToUpload, { recursive: true });

    filenameToUpload = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 10);
    const writer = fs.createWriteStream(pathToUpload + filenameToUpload + '.jpg');

    const response = await Axios({
        url,
        method: 'GET',
        responseType: 'stream',
    });

    response.data.pipe(writer);

    return new Promise((resolve, reject) => {
        writer.on('finish', resolve);
        writer.on('error', reject);
    });
}

async function getDBPath(num, db) {
    await downloadImage('https://picsum.photos/1920/500?random=' + num)
        .then(() => {
            db.collection('posts').findOneAndUpdate(
                { name: 'Что такое Lorem Ipsum? #' + num },
                { $set: { image: pathForDatabase + '/' + filenameToUpload + '.jpg' } },
                { upsert: true },
            );

            console.log('Image ' + num + ' uploaded');

            return true;
        });
}

module.exports = {
    async up(db) {
        for (let i = 0; i < 10; i++) {
            await getDBPath(i, db);
        }
    },
};
