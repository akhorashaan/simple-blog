const Axios = require('axios');
const fs = require('fs');

const currentDateString = new Date();
const year = currentDateString.getFullYear();
const month = currentDateString.getMonth();
const day = currentDateString.getDate();
const pathToUpload = `./uploads/category/${year}/${month}/${day}/`;
const pathForDatabase = `category/${year}/${month}/${day}`;
const categories = ['Музыка', 'Кино', 'Путешествия', 'Литература'];
const slugs = ['music', 'movies', 'travel', 'books'];
let filenameToUpload = '';

async function downloadImage(url) {
    fs.mkdirSync(pathToUpload, { recursive: true });

    filenameToUpload = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 10);
    const writer = fs.createWriteStream(pathToUpload + filenameToUpload + '.jpg');

    const response = await Axios({
        url,
        method: 'GET',
        responseType: 'stream',
    });

    response.data.pipe(writer);

    return new Promise((resolve, reject) => {
        writer.on('finish', resolve);
        writer.on('error', reject);
    });
}

module.exports = {
    async up(db) {
        for (let i = 0; i < categories.length; i++) {
            await downloadImage('https://picsum.photos/1920/500?random=' + i);
            await db.collection('category').insertOne({
                name: categories[i],
                slug: slugs[i],
                image: pathForDatabase + '/' + filenameToUpload + '.jpg',
            });
        }
    },
};
