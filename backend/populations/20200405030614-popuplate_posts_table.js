const body = '<h1>Prave, nequiter, turpiter cenabat;</h1>\n' +
    '\n' +
    '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliter enim explicari, quod quaeritur, non potest. Atqui haec patefactio quasi rerum opertarum, cum quid quidque sit aperitur, definitio est. Nec lapathi suavitatem acupenseri Galloni Laelius anteponebat, sed suavitatem ipsam neglegebat; Claudii libidini, qui tum erat summo ne imperio, dederetur. Id enim volumus, id contendimus, ut officii fructus sit ipsum officium. Intellegi quidem, ut propter aliam quampiam rem, verbi gratia propter voluptatem, nos amemus; Omnia contraria, quos etiam insanos esse vultis. Duo Reges: constructio interrete. Nam adhuc, meo fortasse vitio, quid ego quaeram non perspicis. Illis videtur, qui illud non dubitant bonum dicere -; Negat enim summo bono afferre incrementum diem. </p>\n' +
    '\n' +
    '<h2>Qua ex cognitione facilior facta est investigatio rerum occultissimarum.</h2>\n' +
    '\n' +
    '<p>Octavio fuit, cum illam severitatem in eo filio adhibuit, quem in adoptionem D. Quid, de quo nulla dissensio est? Quae si potest singula consolando levare, universa quo modo sustinebit? Mihi vero, inquit, placet agi subtilius et, ut ipse dixisti, pressius. <b>Fortasse id optimum, sed ubi illud: Plus semper voluptatis?</b> Cur tantas regiones barbarorum pedibus obiit, tot maria transmisit? </p>\n' +
    '\n' +
    '\n' +
    '<p>Magni enim aestimabat pecuniam non modo non contra leges, sed etiam legibus partam. <code>Summum a vobis bonum voluptas dicitur.</code> <code>At ille pellit, qui permulcet sensum voluptate.</code> Portenta haec esse dicit, neque ea ratione ullo modo posse vivi; <mark>Prioris generis est docilitas, memoria;</mark> <b>Ille incendat?</b> </p>\n' +
    '\n' +
    '<ol>\n' +
    '\t<li>Consequens enim est et post oritur, ut dixi.</li>\n' +
    '\t<li>Quantum Aristoxeni ingenium consumptum videmus in musicis?</li>\n' +
    '\t<li>Honesta oratio, Socratica, Platonis etiam.</li>\n' +
    '\t<li>Ad quorum et cognitionem et usum iam corroborati natura ipsa praeeunte deducimur.</li>\n' +
    '</ol>\n' +
    '\n' +
    '\n' +
    '<blockquote cite="http://loripsum.net">\n' +
    '\tQuae quod Aristoni et Pyrrhoni omnino visa sunt pro nihilo, ut inter optime valere et gravissime aegrotare nihil prorsus dicerent interesse, recte iam pridem contra eos desitum est disputari.\n' +
    '</blockquote>\n' +
    '\n' +
    '\n' +
    '<ul>\n' +
    '\t<li>Potius ergo illa dicantur: turpe esse, viri non esse debilitari dolore, frangi, succumbere.</li>\n' +
    '\t<li>Animadverti, ínquam, te isto modo paulo ante ponere, et scio ab Antiocho nostro dici sic solere;</li>\n' +
    '\t<li>Eiuro, inquit adridens, iniquum, hac quidem de re;</li>\n' +
    '\t<li>Ille enim occurrentia nescio quae comminiscebatur;</li>\n' +
    '</ul>\n';

function generateDate(i) {
    const now = new Date;

    return new Date(now - (1000 * 60 * 60 * 2 * i));
}

module.exports = {
    async up(db) {
        for (let i = 0; i < 10; i++) {
            await db.collection('posts').insertOne({
                name: 'Что такое Lorem Ipsum? #' + i,
                slug: 'lorem-ipsum-' + i,
                lead: 'Lorem Ipsum - это текст-"рыба", часто используемый в печати и вэб-дизайне.',
                image: '',
                body: body,
                date: generateDate(i),
            });
        }
    },
};
