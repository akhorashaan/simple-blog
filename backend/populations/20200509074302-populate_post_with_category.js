async function getCategoryIds(db) {
    return await db.collection('category').find({}, { 'name': 0 }).toArray().then(items => {
        const ids = items.map(item => item._id);

        return ids;
    });
};

function getRandomCategory(catIds) {
    return catIds[Math.floor(Math.random() * Math.floor(catIds.length))];
}

async function setCategoryId(num, catIds, db) {
    db.collection('posts').findOneAndUpdate(
        { name: 'Что такое Lorem Ipsum? #' + num },
        { $set: { category: getRandomCategory(catIds) } },
        { upsert: true },
    );
}

module.exports = {
    async up(db) {
        const catIds = await getCategoryIds(db);

        for (let i = 0; i < 10; i++) {
            await setCategoryId(i, catIds, db);
        }
    },
};
