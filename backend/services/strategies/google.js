const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const auth = require('../auth');

passport.use(new GoogleStrategy({
        clientID: process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_SECRET,
        callbackURL: `${process.env.API_URL}user/auth/google/callback`,
        passReqToCallback: true,
    }, async (request, accessToken, refreshToken, profile, done) => {
        return done(null, profile);
    },
));

exports.googleAuth = async (req, res) => {
    passport.authenticate(
        'google',
        {
            session: false,
            scope:
                ['email', 'profile'],
        },
    )(req, res);
};

exports.googleAuthCallback = async (req, res) => {
    passport.authenticate(
        'google',
        {},
        async (err, profile) => {
            const user = {
                name: profile.displayName,
                email: profile.emails[0].value,
                avatar: profile.photos ? profile.photos[0].value : '',
                source: 'google',
                date: Date.now(),
            }

            await auth.findOrCreate(err, user, res);
        })(req, res)
};
