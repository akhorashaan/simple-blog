const passport = require('passport');
const VKontakteStrategy = require('passport-vkontakte').Strategy;
const auth = require('../auth');

passport.use(new VKontakteStrategy({
        clientID: process.env.VK_CLIENT_ID,
        clientSecret: process.env.VK_CLIENT_SECRET,
        callbackURL: `${process.env.API_URL}user/auth/vk/callback`,
        apiVersion: '5.110',
        scope: ['email', 'photo_200'],
        profileFields: ['email', 'photo_200'],
    }, async (accessToken, refreshToken, params, profile, done) => {
        profile.email = params.email;

        return done(null, profile);
    },
));

exports.vkAuth = async (req, res) => {
    passport.authenticate(
        'vkontakte',
        {
            session: false,
            scope: ['status', 'email', 'friends', 'notify', 'photo_200']
        },
    )(req, res);
};

exports.vkAuthCallback = async (req, res) => {
    passport.authenticate(
        'vkontakte',
        {},
        async (err, profile) => {
            const user = {
                name: profile.displayName,
                email: profile.email,
                avatar: profile.photos ? profile.photos[1].value : '',
                source: 'vkontakte',
                date: Date.now(),
            }

            await auth.findOrCreate(err, user, res);
        })(req, res)
};
