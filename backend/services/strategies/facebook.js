const passport = require('passport');
const FacebookStrategy = require('passport-facebook').Strategy;
const auth = require('../auth');

passport.use(new FacebookStrategy({
        clientID: process.env.FACEBOOK_ID,
        clientSecret: process.env.FACEBOOK_SECRET,
        callbackURL: `${process.env.API_URL}user/auth/facebook/callback`,
        profileFields: ['id', 'displayName', 'photos', 'email'],
    }, async (accessToken, refreshToken, params, profile, done) => {
        profile.image = `https://graph.facebook.com/${profile.id}/picture?width=200`;

        return done(null, profile);
    },
));

exports.facebookAuth = async (req, res) => {
    passport.authenticate(
        'facebook',
        {
            authType: 'rerequest',
            session: false,
            scope: ['email'],
        },
    )(req, res);
};

exports.facebookAuthCallback = async (req, res) => {
    passport.authenticate(
        'facebook',
        {},
        async (err, profile) => {
            const user = {
                name: profile.displayName,
                email: profile.emails[0].value,
                avatar: profile.image ? profile.image : '',
                source: 'facebook',
                date: Date.now(),
            }

            await auth.findOrCreate(err, user, res);
        })(req, res)
};
