const User = require('../../models/User');

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

exports.signUp = async (req, res) => {
    const hash = await bcrypt.hash(req.body.pwd, 10);
    const user = { name: req.body.name, email: req.body.email, pwd: hash };

    try {
        await User.create(user);
    } catch (e) {
        if (e.code === 11000) {
            res.status(409);
            res.end('Пользователь с таким именем или адресом электронной почты уже существует');

            return;
        } else {
            res.status(500);
            res.end('Неизвестная ошибка');

            return;
        }
    }

    res.status(200);
    res.send('Регистрация прошла успешно');
};

const validatePwd = async (userPwd, pwd) => {
    return await bcrypt.compare(pwd, userPwd);
};

passport.use('signin', new LocalStrategy({
    usernameField: 'name',
    passwordField: 'pwd',
}, async (name, pwd, done) => {
    try {
        const user = await User.findOne({ name });

        if (!user) {
            return done(null, false, 'Пользователь не найден');
        }

        const isPwdValid = await validatePwd(user.pwd, pwd);

        if (!isPwdValid) {
            return done(null, user, 'Неверный пароль');
        }

        return done(null, user, { message: 'Успешно' });
    } catch (error) {
        return done(error);
    }
}));

exports.signIn = async (req, res) => {
    passport.authenticate(
        'signin',
        { session: false },
        async (err, user, info) => {
            if (err) {
                res.status(500);
                res.send('Неизвестная ошибка');

                return;
            }

            if (!user) {
                res.status(404);
                res.send(info);

                return;
            }

            if (info === 'Неверный пароль') {
                res.status(422);
                res.send(info);

                return;
            }

            const body = { _id: user._id };
            const token = jwt.sign({ user: body }, process.env.JWT_SECRET);

            res.status(200);
            res.send(token);
        })
    (req, res);
};
