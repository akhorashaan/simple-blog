const User = require('../models/User');
const jwt = require('jsonwebtoken');
const imageProcessor = require('./images')

const jwtSignAndSend = (id, err, res) => {
    let token;

    if (!err) {
        token = jwt.sign({ user: { _id: id, success: true } }, process.env.JWT_SECRET);
    } else {
        token = jwt.sign({ user: { err: err, success: false } }, process.env.JWT_SECRET);
    }

    res.redirect(`${process.env.FRONTEND_URL}signin/${token}`);
}

exports.findOrCreate = async (err, user, res) => {
    try {
        const dbUser = await User.findOne({ name: user.name, email: user.email });

        if (dbUser) {
            jwtSignAndSend(dbUser._id, false, res);

            return;
        }

        const newUser = await User.create(user);

        try {
            const avatar = await imageProcessor.uploadAvatar(newUser.avatar, newUser._id);

            await User.findOneAndUpdate(
                { _id: newUser._id },
                { avatar }
            );
        } catch (e) {
            await User.findOneAndUpdate(
                { _id: newUser._id },
                { avatar: '' }
            );
        }

        jwtSignAndSend(newUser._id, false, res);
    } catch (e) {
        console.log(e);
        if (e.code === 11000) {
            jwtSignAndSend(null, 'User is already exist', res);
        } else {
            jwtSignAndSend(null, 'Unknown error', res);
        }
    }
}
