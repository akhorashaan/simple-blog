const Path = require('path');
const fs = require('fs');
const gm = require('gm');

const allowedSized = [['80', '80'], ['580', '200'], ['800', '200'], ['1920', '480']];

const thumbnailSizeIsAllowed = (width, height) => {
    return allowedSized.find((element) => {
        return (element[0] === width && element[1] === height);
    });
};

const parseRequest = (req) => {
    const extension = req.src.slice((req.src.lastIndexOf('.') - 1 >>> 0) + 2);
    const baseFile = `uploads/${req.src}`;
    const pathWithoutExt = baseFile.substr(0, baseFile.length - 4);
    const thumbFile = `${pathWithoutExt}_width${req.width}_height${req.height}.${extension}`;

    return { extension, baseFile, pathWithoutExt, thumbFile };
};

const checkFileExistence = (path) => {
    return fs.existsSync(path);
};

const createThumbAndSend = (parsedRequest, width, height, res) => {
    gm(`./${parsedRequest.baseFile}`)
        .gravity('Center')
        .thumb(
            width,
            height,
            `./${parsedRequest.pathWithoutExt}_width${width}_height${height}.${parsedRequest.extension}`,
            70,
            () => {
                res.sendFile(Path.resolve(parsedRequest.thumbFile), { maxAge: '30d' });
            });
};

exports.sendThumb = async (req, res) => {
    if (req.query.src && req.query.width && req.query.height && thumbnailSizeIsAllowed(req.query.width, req.query.height)) {
        const parsedRequest = parseRequest(req.query);

        if (checkFileExistence(parsedRequest.thumbFile)) {
            res.sendFile(Path.resolve(parsedRequest.thumbFile), { maxAge: '30d' });
        } else {
            createThumbAndSend(parsedRequest, req.query.width, req.query.height, res);
        }
    }
};
