const fs = require('fs');
const gm = require('gm');
const Axios = require('axios');

exports.uploadAvatar = async (path, id) => {
    await fs.mkdirSync('uploads/avatars/', { recursive: true });

    const filename = 'avatars/' + id + '.jpg';
    const writer = await fs.createWriteStream(`uploads/${filename}`);
    const response = await Axios({
        url: path,
        method: 'GET',
        responseType: 'stream',
    });

    await response.data.pipe(writer);
    await gm(`uploads/${filename}`)
        .gravity('Center')
        .thumb(
            200,
            200,
            `uploads/${filename}`,
            80,
            () => {
            },
        );

    return filename;
}
