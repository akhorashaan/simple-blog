# Backend

For backend uses Express.JS, MondoDB and Mongoose.
For development purposes init project by:

```
npm i
```

Base commands:
```
npm run start - starts background with hot reloading

npm run migrate - applying database migrtaions.
                  In process will be created emplty database with cosen name
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
